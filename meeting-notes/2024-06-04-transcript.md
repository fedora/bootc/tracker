﻿poh-xmxm-qyc (2024-06-04 16:02 GMT+2) - Transcript
Attendees
Colin Walters, Eric Curtin, Hristo Marinov, Jason Brooks, Jonathan Lebon, Micah Abbott, Noel Miller, Paul Whalen, Peter Robinson, Timothée Ravier
Transcript
This editable transcript was computer generated and might contain errors. People can also change the text after it was created.
Timothée Ravier: the recording
Timothée Ravier: All right, and to see they're just for sake of recording is that your name and topics you want to discuss today? the meeting agenda
Colin Walters: It starts for turning weight is someone running this.
Jason Brooks: We've started adding items to The Ether pad agenda link.
Colin Walters: All I don't know. Let's dive in Austin. just in the interest of making progress. I will write notes today and I'll make the meeting notes PR. Just do Unless I doubt you do. Yes, awesome done next item. I want to figure out yeah we have an initiative approved but then a comment on the discussion board. I'm feels like we haven't socialized this much or communicated it or talked about what we're doing blog about it just like
Colin Walters: Is our initiative feels like there should be a fedora develop post or something. is there a standard operating procedure for that or I don't know. what's next for the socializ?
Jason Brooks: I don't know what the standard operating procedure is. I can talk to Justin about that. But I agree that we want to make a post on the develop list, beyond, we do the initial thing on the magazine when I think would be good is if we had the sort of the next steps that the things like Come like we've announced that if you are interested in this you can come and join up with us and get involved. But for the people who are watching on the develop list, I think if we have some kind of the next steps of the things that we're working on, that you can get involved with I think that would be worth while and that be good next thing to promote
Colin Walters: Yeah, because one connection here is we have a keynote defconf that's easy related to this next week and definitely wanted to do a call to action here's what we're doing and here's how you can contribute one of the feature the fenora initiative in that and have a list of things to do. Okay. Yep. Looks like Peter.
Colin Walters: you're muted or
Micah Abbott: Here we can hear you.
Peter Robinson: Can you hear me now?
Peter Robinson: Alright my USB things. Going back. when the Fedora change goes out to list. I think that's probably a good spot to actively discuss it because the changes tend to have active watches and active discussion. So that's probably a good spot to possibly reply to that and say, this is what we're doing this release and this is the intentional where we're going on because The fenora changes also tend to be watched by foronics and register and all of that sort of stuff. So it would be a good way of getting things like that picked up by the media. If that's…
00:05:00
Colin Walters: so Yeah,…
Peter Robinson: what you want.
Colin Walters: I guess technically kind of what I was thinking is it feels like we should try and have things so much stabilized by fedora. 41 and so I guess maybe that's The queen we haven't done really good enough job communicating what's this ability of these images and all that stuff and Gucci itself. I think a lot of people realize you guys an official stay a little bit forget about this last time, but I think you could make it just go ahead and try and set up a change for 41 it's just craft that just does that make sense and then also kind of implies I guess we have an issue tracker for our stuff that's targeting for 41 because we talked about this last time and I think that would be almost at our change, but that makes sense.
Jason Brooks: and when you say our school
Peter Robinson: Yeah, I think a lot of those pieces should actually go into the Fedora change so that people can find that information easily.
Jason Brooks: And then there's some different changes that have been referenced to is there a canonical change that we are? The oven mind a particular one.
Colin Walters: Yeah, That one's Linked In the e35 notes too. So. yeah,…
Jason Brooks: Okay.
Colin Walters: so yeah, we get the dnf one posted, but I think it's more like
Colin Walters: yeah, as far as I can see it's basically like image stability bootc clients stability and then the ecosystem, I mean, I'd love to land Anaconda changes related to this there's a bunch of stuff kind of related that and actually a big one probably is bootc image go there and what happens with that which we I know we talked about this but That one definitely needs some thought.
Colin Walters: But I feel like that's the start. It's kind of like for when we have the stable thing that we're trying to maintain for a while.
Noel Miller: Yeah, I'm a hundred percent in agreement with Colin on the Anaconda changes needed. apparently now we create our own custom ISO installation for that and the experience for the users. Especially on the desktop is an early super great. So definitely would appreciate any work that can help those guys out.
Colin Walters: What? yeah, I mean I guess. there's very little relationship between bootc image Builders interface and Anaconda as far as what you can configure for partitioning blueprints versus kickstarts. It's the fact that Anaconda today does not actually use bootc install which means it misses out on things like being able to set kernel arguments in the here, I'll link that. I probably had a tracker for this one. But yeah here I'll put it are you typing right now? Is that your cursor? Let me just overwrite. Yeah, is that guy? Basically, someone's not a showstopper, but it makes us feel in my opinion very inconsistent unless we fix it.
Colin Walters: Yeah, there's a bunch of kind of things related to that.
Jason Brooks: So this sounds like four Fedora for when we would like to have. Stable images or a statement about the stability the images. We'd like to have bootc image Builder DNF changes in Anaconda changes.
00:10:00
Colin Walters: Yeah, I mean.
Jason Brooks: are there
Colin Walters: Yeah, we don't need to land all those I think of those. the bootc and bootc image Builder ones are the most important secondarily Fall by the container images
Colin Walters: yeah, because I guess it might be kind of by foot or affordable. have to we're gonna maintain this for life of our 41 and initiate where it can be checked Downstream too because there's also would like to land in central stream 9 and keep that stuff stable. So.
Jason Brooks: Okay, so the dnf changes there's a change at the door change. What about can we document the Anaconda changes that we would like to see?
Colin Walters: yeah, it's No,…
Jason Brooks: It's someone working on that already.
Colin Walters: and team doesn't have a lot of capacity about trying to see if they can raise this one. It's
Colin Walters: there's a bunch of stuff that people hit. using authenticated Registries with Anaconda is just hacky right now. We have a documented you have to use percent pre just okay, it's fine even but it's just, little less ideal. But actually it's the deeper integration between anaconda and we see they would just angry teenage Builder that would help.
Noel Miller: As silly as this is to in my experience with Anaconda at least the current version of it. There's no real progress for the user to see what's happening either unless you get to installation of flat packs. Which is better supported but the actual deployment of the image. It just says deploying image and…
Peter Robinson: just
Noel Miller: then just sits there, and it's not super obvious unless you're More technical if there's anything that's actually happening. plus the usage of all the Kickstart stuff you have to do too in order to be able to even do the installation period so
Colin Walters: Yeah, the progress bar would require currently so non-trivial glue between the street container side and the Anaconda GUI frontend stuff. I mean it makes total sense, but there' just the sort of core problem is like that code is in Rust and anacondas in Python and it would require some custom gluting. You should not be easy.
Noel Miller: Yeah, I'm not saying any of the changes that need to happen to anaconda or even trivial and the other thing too is that they're trying to move to the web UI which we found it has a spinner that's more obvious. So even if we didn't land like a crew progress bar like the spinner being larger and showing that it's doing something is much better. So I don't know if the web changes or something that we even want to Target though because there's a lot of issues with the web Kickstart files postscripts portions of those files don't work at all currently in the web UI.
Jason Brooks: I think everyone get issues filed or linked for these different Anaconda things and these are the sorts of things when we communicate with the develop list where we can say that these are our priorities and this is like where you could show up to participate
Noel Miller: Does anyone know if there's a public bug tracker for Anaconda besides jira? or is it only being done through jira now because that's where I've been told to submit my issues which can't always be public unfortunately.
Colin Walters: What the year? I mean, yeah, I think anaconda's retighted to the real sick cycle for stuff. But cheers in the red hat here. It can be public.
Colin Walters: Okay,
Timothée Ravier: They recommend Budget on the pitch.
Colin Walters: Yeah. whatever purpose we already have so they don't have issues enabled but they do have discussions so we can maybe just dump I don't know we use the discussion as
00:15:00
Colin Walters: you can edit that or we can have a sub one on our issue tracker for Anaconda. I don't know I mean multiple ways to do it
Colin Walters: I can just copy and
Noel Miller: Sorry, I'm just trying to find a bit more of a more public way. That folks can interact, from the Fedora project because like I said, I mean I've created issues in the jira and then they got attached to a private issue that was related to a Rel release and it was that doesn't necessarily help give visibility to folks to be able to help you guys out more, so that's kind of the thing like with this initiative. I'm hopeful that we can bring in more folks that know python to be able to help with some of these issues. For sure.
Jason Brooks: Maybe we can create an issue on our tracker and then we can figure out the right place to we can link to Bugs in the appropriate place because I'm wondering is Is it actually Jerry now maybe that that bit isn't up to date yet, but that's something we need to follow up on.
Timothée Ravier: Yeah, I created an issue in the bootc tracker and started adding links to. Things that will be mentioned the discussion Around think. There's anything decide the discussions right here. for now
Colin Walters: Yeah, okay, we can yeah, let's just keep stuff there. So I'll just kind of move the stuff from the meeting notes there just so Even in total raw form just to keep it in one place to move on the next one.
Colin Walters: Kiwi, I guess that's you timothée.
Timothée Ravier: yeah, it's just start support for me visitation work. I'm doing on the side. It's kind of related to Anaconda in a sense because I've been working looking at kiwi which is an image life this image Builder be looking at this from the context of federal but also from the greatest desktops. And it works pretty well. recently is it working? And so I got initial work to get bootable containers working to create disk images live isils, or A little bit highly right now but works as well. And so this could be also an option in the future to trade artifacts Yeah, and it's using Anaconda. So yeah.
Timothée Ravier: So right now what's using in Dora was built using kiwi is the continuing matches. All the federal is a one or the cloud images are built and that's it, which is already a lot, but the other installer iso's are not. So the atomic desktops, using the old Lorax will do a project. Which nobody really wants to maintain as far as I know.
Timothée Ravier: the other ones I don't know how that builds.
Colin Walters: Yeah, one thing I did here. I mean. I think it would be better if Anaconda itself was a routine container. Because we don't need another tool. I know it gets meta but it actually would make a ton of sense. Because part of the goal. I have of this project, obviously trying to improve we have use cases where people want to make to install to the hardware. They need to have a custom kernel with custom drivers, So we always hit this with openshift today because you can't use the stock What you need to be able to do is make an ISO that has your kernel.
Colin Walters: That will install a system that has your Chrome, And so the problem we have with the image Builder that will also be there with this kiwi thing is that user experience is actually bifurcated because as someone who wants to customize my OS I can write from photo of UT or from reality run dnf install my K mod great. I get a bootable container. but the iso path if this TV thing is like I need to edit this XML file to you're gonna mean so what I think would be better right is if the two the definition of yeah.
00:20:00
Timothée Ravier: You don't. just sorry to interrupt you, but you don't so the idea is that we description you would have to just specify where you continue image is stored, and it's Creates a new disk image out of it. So You can pass it anything you like and it generates one out of it. So sure the ones will be in federal and fraud will be the generic ones. But then if you want to build your own with your kernel custom writers who's so many thing you will clone the descriptions, maybe change one field for the container image and we're in a command. That's something about it.
Colin Walters: Yeah what I'm saying is that the user's step here. And what I'm proposing is we would have quite adios / Fedora slash Anaconda. Colin 41 Anaconda would show up as a boot scene container image. the iso that we generate would literally just be materializing that disconnecting just instead of The Lorax templates, that you're talking about before or having an XML thing with it's basically don't go some bespoke list of packages to a disk image go container build to just Always right. So the user here is user experience of…
Timothée Ravier: but don't
Colin Walters: what I'm talking about would be they could literally say from the Anaconda ISO as a container building. Anyways, yeah, we
Timothée Ravier: That's exactly what I'm building. it's exactly…
Colin Walters: Okay.
Timothée Ravier: what I'm building right now for this Pathways I've got the Atomic desktop image. I have a layer that is included in Anaconda and all the live stuff. And then I think this Leo damage and…
Colin Walters: Okay.
Timothée Ravier: a pass it to Kiwi and I get a live ISO within the contact print store inside of it.
Colin Walters: That's cool. Yeah, that's definitely right. Yeah, okay.
Noel Miller: I guess one question that I had so we have several projects and Fedora that are using kiwi. to I would assume there's a desire to standardize on one thing rather than multiple different ways of isos or generating disk images, has there been any talk as far as standardizing and kiwi or is this just something that we're talking about potentially utilizing for? the atomic stuff
Timothée Ravier: as you
Peter Robinson: There's a lot of projects starting to use OS build as well the problem I think.
Noel Miller: That I was gonna say, yeah.
Peter Robinson: Part of the problem with anyway is I believe.
Timothée Ravier: this work
Peter Robinson: The soothing people, I no longer maintaining it, so I'm not sure.
Peter Robinson: how that works in terms of longevity there. I know the OS build people are working to make things more flexible there and I was speaking to them about in the context of edge this morning, so there's a bunch of different tools and I'm not sure how we might the decision.
Timothée Ravier: Just skyfi here. This is two weeks old. I just done that. It's not pretty but it's an investigation because we're going to use that anyway for the Sie work. So kiwis likely not going away anytime soon in fedora. And so yeah, it's just an option
Jonathan Lebon: But have there been any discussions around using Boosie image Builder and Fedora and fraud? Because that also can build Anaconda isos confused I guess from my point of view I think it would be really really good if the way the official Isis were built are the same way a user's expected to build them. So ideally the production one isn't build with kiwi but users are documented to use, bootc image Builder because yeah, that's just gonna invite friction there between the two differences.
Peter Robinson: an image Builder is supported in Fedora and I think anything that happens within realm will be image Builder base to write. so from that perspective, there is at least a Dev team working on stuff. So
00:25:00
Timothée Ravier: Yeah, just like we're in the drug Community bootc brought container initiative here. So decision for real are essentially not our concerns, will those whatever it wants. It's the committee project here. So if we think a solution is better than another we don't have
Peter Robinson: it's more about longevity of support is the raising I mentioned that.
Timothée Ravier: a great
Jonathan Lebon: so if let is already wired in Fedora that feels like That's something to investigate right? We already have that paved. So if we just need to add Definitions pipelines whatever to build an anaconda ISO and that's the same way would be done in for a user's Custom ISO. I feel like that that could be a path forward to
Colin Walters: so this is super confusing to talk about but the thing the hosted image Builder service that Prodigy can call to you I don't believe supports container images would contain images like bootc image Builder. And image Builder yes, there's some code in common, but they're actually pretty different at that high level. Yeah.
Peter Robinson: Yeah, the service inventory is evolving quite quickly though. So there'll be some changes coming up probably the end of this month that will evolve things quite a bit.
Jonathan Lebon: yeah, this goes back to I feel image Builder and boost image brother should just be using the same stuff underneath and you just think it shouldn't matter which one you're using for. it's just a matter of sort of ux convenience on top around the OS build manifest definition.
Peter Robinson: Yep.
Jonathan Lebon: So ideally we wouldn't have that kind of Delta there.
Jason Brooks: Okay, we are at time, I'm gonna take an action to sort of get together a communication to send out to the list which will entail kind of. Some clarity on these issues. I think that what's emerging here is a list of some issues that do need to be targeted that we can point to and invite people to get involved in. So I'll gather together that now I'll ask about it in the Matrix room while I'm doing that and we can just feel good. Maybe we can talk about it on Thursday and then send it out and direct some attention.
Jason Brooks: All…
Noel Miller: Yeah. That's good.
Jason Brooks: All right, cool. thanks everyone for coming and we will see you soon.
Noel Miller: Hi, everyone.
Meeting ended after 00:28:29 👋
