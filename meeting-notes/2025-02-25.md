```
=====================================
# #meeting-1:fedoraproject.org: fedora_bootc_initiative
=====================================

Meeting started by @jbrooks:matrix.org at 2025-02-25 15:01:01



Meeting summary
---------------
* TOPIC: roll call (@jbrooks:matrix.org, 15:01:17)
* TOPIC: Action items from last meeting (@jbrooks:matrix.org, 15:05:14)
* TOPIC: Base Image Discussion (@jbrooks:matrix.org, 15:11:10)
* TOPIC: Wish List Items for bootc from FCOS (@jbrooks:matrix.org, 15:24:12)
    * ACTION: jbrooks to ping dnf folks for video mtg next week (@jbrooks:matrix.org, 15:50:10)

Meeting ended at 2025-02-25 15:53:33

Action items
------------
* jbrooks to ping dnf folks for video mtg next week 

People Present (lines said)
---------------------------
* @dustymabe:matrix.org (53)
* @walters:fedora.im (26)
* @jbrooks:matrix.org (22)
* @jlebon:fedora.im (16)
* @zodbot:fedora.im (6)
* @meetbot:fedora.im (2)
* @rsturla:fedora.im (1)
* @hricky:fedora.im (1)
```
[full meeting log here](https://meetbot-raw.fedoraproject.org/meeting-1_matrix_fedoraproject-org/2025-02-25/fedora-bootc-initiative.2025-02-25-15.01.log.html)