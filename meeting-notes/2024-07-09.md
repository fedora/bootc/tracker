# bootable containers initiative meeting - 2024-07-09

## Attendees

- Timothée Ravier
- Hristo Marinov
- Noel Miller
- Jonathan Lebon
- Jason Brooks
- Josh Gwosdz
- Paul Whalen
- David Cantrell
- Colin Walters
- Joseph Marrero

## Agenda

https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Meeting

## Decide who will be writting notes and who will make the meeting notes PR

- ?

## dnf

- https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Area%3A%3Adnf
- https://issues.redhat.com/browse/SWM-1129

## composefs

- Do we have composefs enabled by default?
    - Yes for bootc images
    - Not yet for all other variants (CoreOS, IoT, Atomic Desktops)
    - https://gitlab.com/fedora/bootc/tracker/-/issues/11
- https://gitlab.com/fedora/bootc/tracker/-/issues/26
- composefs and grub
    - https://fedoraproject.org/wiki/Changes/ComposefsAtomicCoreOSIoT#Feedback
    - https://gitlab.com/fedora/ostree/sig/-/issues/35#note_1986555833
    - https://github.com/containers/bootc/commit/d46b072cd218ab114a01d604592b5c42cc1b8866
    - nmbl https://www.youtube.com/watch?v=ywrSDLp926M
    - https://lwn.net/Articles/979789/
        - https://lwn.net/Articles/981149/
 - Discussion of nmbl

## dnf & bootc upcoming meeting

- Will be announced in the matrix channel
